#!/usr/bin/python3
# -*- coding: latin-1 -*-
#
# © 2018 Microchip Technology Inc. and its subsidiaries.
#
# Subject to your compliance with these terms, you may use Microchip software
# and any derivatives exclusively with Microchip products. It is your
# responsibility to comply with third party license terms applicable to your
# use of third party software (including open source software) that may
# accompany Microchip software.
#
# THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER
# EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
# IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
# PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT,
# SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR
# EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
# EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
# FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL
# LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED
# THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR
# THIS SOFTWARE.

# Serial bootloader script for Microchip tinyAVR 0-, 1-series and megaAVR 0-series MCUs
#
# Futurized to support python 2 and 3.
# Download Python from https://www.python.org/downloads/ 
#
from __future__ import print_function
try:
	import sys
	from builtins import bytes, int
	import argparse
	from serial import Serial
	from intelhex import IntelHex
except ImportError:
    sys.exit("""ImportError: You are probably missing some modules.
To add needed modules, run 'python -m pip install -U future pyserial intelhex'""")

# Generate help and use messages 
parser = argparse.ArgumentParser(
description='Serial Bootloader Script for eHaJo Fumetractor',
epilog='Example: tiny_uploader.py fumetractor.hex COM7',
formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('file', help='path to fumetractor hex-file')
parser.add_argument('comport', help='UART COM port')
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Command line arguments
file = sys.argv[1]
flashsize = 16384
comport = sys.argv[2]

# Load application file and convert to byte array
# The hex file must have a start address corresponding to BOOTEND*256
# Array is padded with 0xff to fill entire application/appdata section
ih = IntelHex()
fileextension = file[-3:]
ih.loadfile(file, format=fileextension)
append = ih.maxaddr()
if (append > flashsize):
    print ("Error: Flashsize input: %#06x, " % (flashsize) +
           "Minimum size needed: %#06x." % (append))
    sys.exit(1)
appstart = ih.minaddr()
app = ih.tobinarray(end=flashsize-1)


# Init serial communication
try: 
	uart = Serial(comport, baudrate=19200, timeout=1)
except:
    print ("Error opening serial port!")
    exit()

if uart.isOpen():
	# Send application to device. Each byte is is echoed for validation.
	size = len(app)
	failed = 0
	counter = 0

	print ("Firmware-Uploader www.eHaJo.de")
	print ("Uploading Fumetractor-Firmware (", size, "bytes... )")
	print ("This takes up to 5 minutes.")
	print ("Don't power off or disconnect cable during this process!")
	print ("")
	print (" ____________       _____|")
	print ("| |        | |     |     |\\")
	print ("| |   FT   | |     |     | |")
	print ("| |   FW   | |     |     | |")
	print ("| |________| |     |     |/")
	print ("|   ______   |     |     |")
	print ("|  |    | |  | USB |     |")
	print ("|__|____|_|__| ==> |_____|")
	print ("")
	
	for byte in app:
		byte = bytes([byte])
		try: 
			uart.write(byte)
		except:
			print ("serial port connection lost!")
			exit()
		tmp_byte = uart.read(1)
		#if (tmp_byte != byte):
			#failed = 1
			#break
		counter += 1
		print ("\r%.1f%%" % (float(counter)/size*100), end='')

	# Check reason for exiting loop and print debug info
	if failed:
		print ("\nFailed at address %#06x" % (counter+appstart))
		tmp_byte = int.from_bytes(tmp_byte, byteorder="little")
		byte = int.from_bytes(byte, byteorder="little")
		print ("Value %#04x echoed, expected %#04x" % (tmp_byte, byte))
	else:
		print ("\nOK")

	# Device will reset and jump to application when flash is successfully programmed,
	# or it can be externally reset or power-cycled.
else:
    print ("cannot open serial port ")