-- Grant Searle's Multicomp as described here:
-- http://searle.x10host.com/Multicomp/index.html
-- 
-- http://land-boards.com/blwiki/index.php?title=EP2C5-DB
--
-- 6502 CPU
--  16.6 MHz
--  56 KB SRAM
--  Microsoft BASIC in ROM
--      56,831 bytes free
-- ANSI Video Display Unit
--      80x25 character display
--      2/2/2 - R/G/B output
-- PS/2 Keyboard
--      F1 key switches between VDU and Serial port
--          Default is VDU
--      F2 key switches baud rate between 300 and 115,200
--          Default is 115,200 baud
--
-- Memory Map
--      x0000-xDFFF - 56KB SRAM
--      xE000-xFFFF - 8KB BASIC in ROM
--  I/O
--      XFFD0-FFD1 VDU
--      XFFD2-FFD3 ACIA

library ieee;
use ieee.std_logic_1164.all;
use  IEEE.STD_LOGIC_ARITH.all;
use  IEEE.STD_LOGIC_UNSIGNED.all;

entity M6502_VGA is
    port(
        i_n_reset   : in std_logic;
        i_clk_50    : in std_logic;
        
        i_rxd       : in std_logic;
        o_txd       : out std_logic;
        o_n_rts     : out std_logic;
        
        videoR0     : out std_logic;
        videoR1     : out std_logic;
        videoG0     : out std_logic;
        videoG1     : out std_logic;
        videoB0     : out std_logic;
        videoB1     : out std_logic;
        o_vid_hSync : out std_logic;
        o_vid_vSync : out std_logic;
        
        io_ps2Clk   : inout std_logic;
        io_ps2Data  : inout std_logic;
        
        Pin25       : out std_logic;
        Pin31       : out std_logic;
        --Pin41       : out std_logic;
        Pin40       : out std_logic;
        Pin47       : out std_logic;
        Pin42       : out std_logic;
        Pin58       : out std_logic;
        Pin44       : out std_logic;
        Pin55       : out std_logic;
        Data_out    : out std_logic_vector(7 downto 0); -- Test output
        LEDR        : out std_logic;
        LEDM        : out std_logic;
        LEDL        : out std_logic;
        -- 128KB SRAM (56KB used)
        io_extSRamData      : inout std_logic_vector(7 downto 0) := (others=>'Z');
        o_extSRamAddress    : out std_logic_vector(16 downto 0);
        io_n_extSRamWE      : out std_logic := '1';
        io_n_extSRamCS      : out std_logic := '1';
        io_n_extSRamOE      : out std_logic := '1'
    );
end M6502_VGA;

architecture struct of M6502_VGA is

    signal w_R1W0           : std_logic;
    signal w_cpuAddress     : std_logic_vector(15 downto 0);
    signal w_cpuDataOut     : std_logic_vector(7 downto 0);
    signal w_cpuDataIn      : std_logic_vector(7 downto 0);
    
    signal w_MonitorRomData     : std_logic_vector(7 downto 0);
    signal w_BootRomData     : std_logic_vector(7 downto 0);
    signal w_P0_RomData     : std_logic_vector(7 downto 0);
    signal w_P1_RomData     : std_logic_vector(7 downto 0);
    signal w_P2_RomData     : std_logic_vector(7 downto 0);
    signal w_P3_RomData     : std_logic_vector(7 downto 0);
    signal w_VDUDataOut     : std_logic_vector(7 downto 0);
    signal w_aciaDataOut    : std_logic_vector(7 downto 0);
    signal w_aciaDataIn     : std_logic_vector(7 downto 0);
    signal w_ramDataOut     : std_logic_vector(7 downto 0);
    
    signal w_latch_User     : std_logic_vector(7 downto 0);
    signal w_latch_Bank     : std_logic_vector(7 downto 0);
    signal w_latch_U7       : std_logic_vector(7 downto 0);
    
    signal w_n_memWR        : std_logic;
    
    signal w_n_MonitorRomCS     : std_logic :='1';
    signal w_n_BootRomCS     : std_logic :='1';
    signal w_n_VDUCS        : std_logic :='1';
    signal w_n_IRQ_address        : std_logic :='1';
    signal w_n_ramCS        : std_logic :='1';
    signal w_n_aciaCS       : std_logic :='1';
    signal w_io_enable       : std_logic :='0';
    signal w_n_userLatchCS  : std_logic :='1' ;
    signal w_n_bankLatchCS  : std_logic :='1' ;
    signal w_n_P0_RomCS     : std_logic :='1' ;
    signal w_n_P1_RomCS     : std_logic :='1' ;
    signal w_n_P2_RomCS     : std_logic :='1' ;
    signal w_n_P3_RomCS     : std_logic :='1' ;
    signal w_n_MuxCS        : std_logic :='1' ;
    signal w_n_VIACS        : std_logic :='1' ;
--  signal w_n_LatCS            : std_logic :='1';
--  signal w_n_LatCS_Read   : std_logic :='1';
    
    signal w_serialClkCount : std_logic_vector(15 downto 0);
    signal w_serClkCt_d     : std_logic_vector(15 downto 0);
    signal w_w_serClkEn     : std_logic;

    signal w_cpuClkCt       : std_logic_vector(5 downto 0); 
    signal w_cpuClkPh       : std_logic; 
    signal w_cpuClk         : std_logic;
    signal w_ACIAClk        : std_logic;

    signal w_latBits        : std_logic_vector(7 downto 0);
    signal w_fKey1          : std_logic;
    signal w_fKey2          : std_logic;
    signal w_fKey3          : std_logic;
    signal w_Sync           : std_logic;
    signal n_CPU_IRQ        : std_logic := '1';
    signal p_o_IRQ          : std_logic := '0';
    signal n_Interr8        : std_logic_vector(7 downto 0) := (others=>'1');
    signal n_InterrSRC      : std_logic_vector(2 downto 0) := (others=>'1');
    signal n_InterrSRC_latched : std_logic_vector(7 downto 0) := (others=>'1');
    signal w_funKeys        : std_logic_vector(12 downto 0);
    signal count            : integer:=1;
    signal tmp              : std_logic := '0';

    signal serialClkCount   : unsigned(15 downto 0);
    signal clken_counter    :   unsigned(3 downto 0);
    signal mhz1_clken       :   std_logic; -- Used by 6522    
    signal mhz8_clken       :   std_logic; -- Used by 6522    
    signal mhz4_clken       :   std_logic; -- Used by 6522    
    signal mhz16_clken       :   std_logic;    
-- User VIA signals
    signal io_VIAData      :   std_logic_vector(7 downto 0);
    signal io_via_do_oe_n :   std_logic;
    signal io_via_ca1_in  :   std_logic := '0';
    signal io_via_ca2_in  :   std_logic := '0';
    signal io_via_ca2_out :   std_logic;
    signal io_via_ca2_oe_n    :   std_logic;
    --signal io_via_pa_in   :   std_logic_vector(7 downto 0);
    signal io_via_pa_out  :   std_logic_vector(7 downto 0)  := (others=>'1');
    signal io_via_pa_oe_n :   std_logic_vector(7 downto 0);
    signal io_via_cb1_in  :   std_logic := '0';
    signal io_via_cb1_out :   std_logic;
    signal io_via_cb1_oe_n    :   std_logic;
    signal io_via_cb2_in  :   std_logic := '0';
    signal io_via_cb2_out :   std_logic;
    signal io_via_cb2_oe_n    :   std_logic;
    signal io_via_pb_in   :   std_logic_vector(7 downto 0);
    signal io_via_pb_out  :   std_logic_vector(7 downto 0)  := (others=>'1');
    signal io_via_pb_oe_n :   std_logic_vector(7 downto 0);
    signal io_n_BootRomWE :   std_logic := '1';
begin
    -- ____________________________________________________________________________________
-- RAM Mapping
    o_extSRamAddress    <=
        '0'&(not w_latch_User(0))&w_cpuAddress(14 downto 0) when w_cpuAddress(15) = '0' else   -- User 0 gets 0-7fff, User1 8000-ffff (0000-7FFF)
        "1101"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "100" else -- Common ROM (8000-9FFF)
        "1000"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"FE"  else -- Bank 0 (A000-BFFF)
        "1001"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"FD"  else -- Bank 1 (A000-BFFF)
        "1010"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"FB"  else -- Bank 2 (A000-BFFF)
        "1011"&w_cpuAddress(12 downto 0)                    when w_cpuAddress(15 downto 13) = "101" and w_latch_Bank = X"F7"  else -- Bank 3 (A000-BFFF)
        '1'&w_cpuAddress                                    when w_cpuAddress > X"BFFF" and w_cpuAddress < X"FFFA" else -- Common RAM (C000-FFF9)
        '1'&X"FF"&"111100"&w_latch_U7(2)&w_cpuAddress(0)    when w_cpuAddress(15 downto 1) = X"FFF"&"101" and w_fKey3 = '1' else -- FFFA-B (NMI) gets mapped to FFF0-1 to FFF6-7
        '1'&X"FF"&"1111110"&w_cpuAddress(0)                 when w_cpuAddress(15 downto 1) = X"FFF"&"110" and w_fKey3 = '1' else -- FFFC-D (RST) gets mapped to FFFC-D to FFFE-F
        '1'&X"FF"&"1110"&n_InterrSRC_latched(2 downto 0)&w_cpuAddress(0) when w_cpuAddress(15 downto 1) = X"FFF"&"111" and w_fKey3 = '1' else -- FFFE (IRQ) gets mapped to FFE0 to FFEE
        '1'&w_cpuAddress                                    when w_cpuAddress > X"FFF9"  and w_fKey3 = '0' ;

    io_extSRamData      <= w_cpuDataOut when ((w_R1W0='0') and (w_n_ramCS = '0')) else
                              (others => 'Z');
    io_n_extSRamWE      <= not((not w_n_ramCS) and (not mhz1_clken) and (not w_R1W0));
    io_n_extSRamOE      <= not((not w_n_ramCS) and (not mhz1_clken) and      w_R1W0);
    io_n_extSRamCS      <= not((not w_n_ramCS) and (not mhz1_clken));
    
-- Chip Selects
    w_n_ramCS       <=  '0' when w_cpuAddress(15) = '0'                else -- x0000-x7FFF (32KB) User RAM
                        '0' when w_cpuAddress(15 downto 14) = "10"     else -- x8000-xBFFF (16KB) Common ROM + Banked ROM
                        '0' when w_cpuAddress(15 downto 13) = "110"    else -- xC000-xDFFF (8KB)  Common RAM Part I
                        '0' when w_cpuAddress(15 downto 12) = "1110"   else -- xE000-xEFFF (4KB)  Common RAM Part II
                        '0' when w_cpuAddress(15 downto 11) = "11110"  else -- xF000-xF7FF (2KB)  Common RAM Part III
                        '0' when w_cpuAddress(15 downto 10) = "111111" and w_fKey2 = '1' else -- xFC00-xFFFF (1KB)  MUC ROM
                        '1';
    --w_n_IRQ_address <= '1';
    w_n_VDUCS       <= '1';

    --w_n_VIACS       <= '0' when ((w_cpuAddress(15 downto 4)  = "111110001111"))    else '1'; -- Xf8f0-f8ff
    --w_n_MuxCS       <= '0' when ((w_cpuAddress(15 downto 4)  = "111110001101"))    else '1'; -- Xf8d0-f8df
    --w_n_bankLatchCS <= '0' when ((w_cpuAddress(15 downto 3)  = "1111100011100"))   else '1'; -- Xf8e0-f8e7
    --w_n_userLatchCS <= '0' when ((w_cpuAddress(15 downto 3)  = "1111100011101"))   else '1'; -- Xf8e8-f8ef
    --w_n_aciaCS      <= '0' when ((w_cpuAddress(15 downto 2)  = "11111000000100"))  else '1'; -- XF810-XF813

    --w_n_MonitorRomCS  <= '0' when   w_cpuAddress(15 downto 11) = "10000" else '1'; -- x8000-x87FF (2KB)
    w_n_BootRomCS     <= '0' when   w_cpuAddress(15 downto 9) = "1111111" else '1'; -- xFE00-xFFFF (1KB)
    --io_n_BootRomWE      <= not((not w_n_BootRomCS) and (not mhz1_clken) and (not w_R1W0));
    --
-- IO Area
    w_io_enable <= '1' when ((w_cpuAddress(15 downto 8)  = X"F8"))    else '0'; -- Xf8xx
    process (w_cpuAddress,w_io_enable)
    begin
        w_n_aciaCS <= '1';
        w_n_MuxCS <= '1';
        w_n_bankLatchCS <= '1';
        w_n_userLatchCS <= '1';
        w_n_VIACS <= '1';
        if w_io_enable = '1' then
            case w_cpuAddress(7 downto 2) is
            when "000101" => w_n_aciaCS <= '0';      -- F814 - F817
            when "110100" => w_n_MuxCS <= '0';       -- F8D0 - F8D3
            when "110101" => w_n_MuxCS <= '0';       -- F8D4 - F8D7
            when "110110" => w_n_MuxCS <= '0';       -- F8D8 - F8DB
            when "110111" => w_n_MuxCS <= '0';       -- F8DC - F8DF
            when "111000" => w_n_bankLatchCS <= '0'; -- F8E0 - F8E3
            when "111001" => w_n_bankLatchCS <= '0'; -- F8E4 - F8E7
            when "111010" => w_n_userLatchCS <= '0'; -- F8E8 - F8EB
            when "111011" => w_n_userLatchCS <= '0'; -- F8EC - F8EF
            when "111100" => w_n_VIACS <= '0';       -- F8F0 - F8F3
            when "111101" => w_n_VIACS <= '0';       -- F8F4 - F8F7
            when "111110" => w_n_VIACS <= '0';       -- F8F8 - F8FB
            when "111111" => w_n_VIACS <= '0';       -- F8FC - F8FF

            when others =>
                null;
            end case;
        end if;
    end process;

-- Bus Isolation
    w_cpuDataIn <=
        --w_MonitorRomData when w_n_MonitorRomCS = '0' and w_fKey2 = '0' else
        w_VDUDataOut     when w_n_VDUCS      = '0' else
        w_aciaDataOut    when w_n_aciaCS     = '0' else
        X"1F" when w_cpuAddress = X"F818" else          -- ACIA DIP Switches 19200Bd, 8n1
        --X"1F" when w_cpuAddress = X"F828" else          -- ACIA DIP Switches 19200Bd, 8n1
        io_extSRamData   when w_n_ramCS      = '0' else
        io_VIAData       when w_n_VIACS      = '0' else
        w_BootRomData    when w_n_BootRomCS    = '0' and w_fKey2 = '0' else      -- HAS TO BE AFTER ANY I/O READS - why?
        x"FF";

    --w_n_memWR           <= not(mhz1_clken) nand (not w_R1W0);
    --w_aciaDataIn <= w_cpuDataOut when w_n_aciaCS  = '0'   else x"FF";
    VIA: entity work.M6522 -- U12 0xF8F0 (for Interrupt Masking, 50Hz Signal,..)
        port map (
        I_RS            => w_cpuAddress(3 downto 0),
        I_DATA          => w_cpuDataOut,
        O_DATA          => io_VIAData,
        O_DATA_OE_L     => io_via_do_oe_n,
        I_RW_L          => w_R1W0,
        I_CS1           => '1',
        I_CS2_L         => w_n_VIACS,
        O_IRQ_L         => n_Interr8(6),  -- map to IRQ6
        -- port a
        I_CA1           => io_via_ca1_in,
        I_CA2           => '0', --io_via_ca2_in,
        -- O_CA2           => io_via_ca2_out,
        -- O_CA2_OE_L      => io_via_ca2_oe_n,
        I_PA            => (others => '0'), --io_via_pa_in,
        O_PA            => io_via_pa_out,
        O_PA_OE_L       => io_via_pa_oe_n,
        -- port b
        I_CB1           => '0', --io_via_cb1_in,
        -- O_CB1           => io_via_cb1_out,
        -- O_CB1_OE_L      => io_via_cb1_oe_n,
        I_CB2           => '0', --io_via_cb2_in,
        -- O_CB2           => io_via_cb2_out,
        -- O_CB2_OE_L      => io_via_cb2_oe_n,
        I_PB            => io_via_pb_in,
        O_PB            => io_via_pb_out,
        O_PB_OE_L       => io_via_pb_oe_n,
        I_P2_H          => not mhz1_clken,
        RESET_L         => i_n_reset,
        ENA_4           => '1', --not mhz4_clken,
        CLK             => mhz4_clken --not i_clk_50
        );
    
    io_via_ca1_in <= io_via_pb_out(7);

    IRQENC: entity work.V74x148 -- U10 for Interrupts
        port map(
            --EI_L            => mhz1_clken,
            I_L             => (n_Interr8 or io_via_pa_out), -- mask Interrupts with 6522s Port A
            A_L             => n_InterrSRC,
            EO_L            => p_o_IRQ --n_CPU_IRQ
                -- GS_L            => 
        );

    Pin40 <= not p_o_IRQ;              --DEBUG Output all the Interrupts (connected to MCU)
    Pin44 <= '0' when (w_cpuAddress = X"8810") else '1';
    n_CPU_IRQ <= not p_o_IRQ;
    --Pin44 <= n_Interr8(6);
    Pin47 <= n_Interr8(5);             -- DEBUG Output ACIAs Interrupt
    Data_out <= n_Interr8 or io_via_pa_out; --DEBUG mask Interrupts with 6522s Port A
    Pin42 <= io_via_pb_out(7);

    CPU : entity work.T65
        port map(
            Enable          => '1',           -- 
            Mode            => "00",          -- "00" => 6502, "01" => 65C02, "10" => 65C816
            Res_n           => i_n_reset,     -- 
            clk             => mhz1_clken,      -- CPU uses rising edge
            Rdy             => '1',           -- 
            Abort_n         => '1',           -- 
            IRQ_n           => n_CPU_IRQ,      -- 
            NMI_n           => '1',           -- 
            SO_n            => '1',           -- 
            R_w_n           => w_R1W0,        -- 
            Sync            => w_Sync,
            A(15 downto 0)  => w_cpuAddress,  -- 
            DI              => w_cpuDataIn,   -- 
            DO              => w_cpuDataOut   -- 
        );

    UART : entity work.ACIA -- 0xF810 for User 0 (only one ACIA for now)
        port map(
            RESET   => i_n_reset,
            PHI2    => not mhz1_clken,
            CS      => w_n_aciaCS ,
            --CS      => not (not w_n_aciaCS and w_cpuClk),
            RWN     => w_R1W0,
            RS      => w_cpuAddress(1 downto 0),
            DATAIN  => w_cpuDataOut,
            DATAOUT => w_aciaDataOut,
            XTLI    => w_ACIAClk,
            RTSB    => o_n_rts,
            CTSB    => '0',
          --DTRB    => 
            RXD     => i_rxd,
            TXD     => o_txd,
            IRQn    => n_Interr8(5) -- map to IRQ5
        );

    MUX: entity work.AddressableLatch -- U7 0xF8D0-0xF8DF (for NMI Masking, Idle LED, NMI Vector shift,...)
        port map(
            dataIn          => w_cpuAddress(3),
            clock           => not mhz1_clken or w_R1W0,
            addrIn          => w_cpuAddress(2 downto 0),
            load            =>  w_n_MuxCS,
            clear           => '1', --i_n_reset
            dataOut8        => w_latch_U7
        );
    
    USRLATCH: entity work.OutLatch -- U5 0xF8E8 (User Switch)
        port map(
            dataIn8         => w_cpuDataOut,
            clock           => mhz1_clken or w_R1W0,
            load            => w_n_userLatchCS,
            clear           => '1', --i_n_reset, WHY doesn't this work??
            latchOut        => w_latch_User
        );

    IRQLATCH: entity work.OutLatch -- U15 (IRQ Latch)
        port map(
            dataIn8         => "11111"&n_InterrSRC,
            clock           => not mhz1_clken,
            load            => not w_Sync,
            clear           => '1', --i_n_reset, WHY doesn't this work??
            latchOut        => n_InterrSRC_latched
        );

    BNKLATCH: entity work.OutLatch -- U4 0xF8E0 (Bank Switch)
        port map(
            dataIn8         => w_cpuDataOut,
            clock           => mhz1_clken or w_R1W0,
            load            => w_n_bankLatchCS,
            clear           => '1', --i_n_reset,
            latchOut        => w_latch_Bank -- (when i_n_reset = '0') else (others => '1')--w_latch_Bank
        );

    BOOTROM: entity work.intelHEX_dl -- 512B 0xFE00-0xFFFF (primary downloader, can be switched out with "F2")
        port map(
            address => w_cpuAddress(8 downto 0),
            clock   => i_clk_50,
            q       => w_BootRomData
        );

    VDU: entity work.SBCTextDisplayRGB
        generic map ( 
            EXTENDED_CHARSET => 0
        )
        port map (
            n_reset  => i_n_reset,
            clk      => i_clk_50,
    
            -- RGB video signals
            hSync    => o_vid_hSync,
            vSync    => o_vid_vSync,
            videoR1  => videoR1,
            videoR0  => videoR0,
            videoG1  => videoG1,
            videoG0  => videoG0,
            videoB1  => videoB1,
            videoB0  => videoB0,
    
            n_WR     => w_n_VDUCS or mhz1_clken or w_R1W0,
            n_RD     => w_n_VDUCS or mhz1_clken or (not w_R1W0),
            --n_int => n_int1,
            regSel   => w_cpuAddress(0),
            dataIn   => w_cpuDataOut,
            dataOut  => w_VDUDataOut,
            ps2Clk   => io_ps2Clk,
            ps2Data  => io_ps2Data,
            FNkeys   => w_funKeys
        );

    FNKey1Toggle: entity work.Toggle_On_FN_Key  
        port map (      
            FNKey => w_funKeys(1),
            clock => i_clk_50,
            n_res => i_n_reset,
            latchFNKey => w_fKey1
        );  

    FNKey2Toggle: entity work.Toggle_On_FN_Key  
        port map (      
            FNKey => w_funKeys(2),
            clock => i_clk_50,
            n_res => '1', --i_n_reset,
            latchFNKey => w_fKey2
        );
        
    FNKey3Toggle: entity work.Toggle_On_FN_Key  
        port map (      
            FNKey => w_funKeys(3),
            clock => i_clk_50,
            n_res => '1', --i_n_reset,
            latchFNKey => w_fKey3
        );
        
-- SUB-CIRCUIT CLOCK SIGNALS 
    process(mhz16_clken,i_n_reset)
    begin
        if i_n_reset = '0' then
            clken_counter <= (others => '0');
        elsif rising_edge(mhz16_clken) then
            clken_counter <= clken_counter + 1;
            
        end if;
    end process;

    --<s <= clken_counter(0) and clken_counter(1) and clken_counter(2); -- 7/15/23/31
    --Pin44  <= clken_counter(0) and clken_counter(1) and clken_counter(2);
    mhz4_clken <= clken_counter(1); -- 1,5,9,13 16.6/4 = 4.2
    --Pin40      <= mhz4_clken;
    --Pin42      <= mhz4_clken;
    --Pin55      <= mhz4_clken;
    --mhz1_clken <= (not (clken_counter(0) and clken_counter(1) and clken_counter(2)));
    mhz1_clken <= clken_counter(3);
    --Pin44      <= (not (clken_counter(0) or clken_counter(1) or clken_counter(2)));
    --Pin55 <= mhz1_clken;
    --Pin47 <= mhz1_clken;
    --Pin58 <= mhz1_clken;
    --cpu_clken <= not (clken_counter(0) or clken_counter(1) or clken_counter(2) or clken_counter(3)); -- 0/16
    --cpu_clken <= cpu_cycle and not cpu_cycle_mask;
    
    process (i_clk_50)
    begin
        if rising_edge(i_clk_50) then

            if w_cpuClkCt < 2 then -- 4 = 10MHz, 3 = 12.5MHz, 2=16.6MHz, 1=25MHz
                w_cpuClkCt <= w_cpuClkCt + 1;
            else
                w_cpuClkCt <= (others=>'0');
            end if;
            if w_cpuClkCt < 2 then -- 2 when 10MHz, 2 when 12.5MHz, 2 when 16.6MHz, 1 when 25MHz
                mhz16_clken <= '0';
                --Pin44  <=  '0';
            else
                mhz16_clken <= '1';
                --Pin44  <=  '1';
            end if; 
        end if; 
    end process;

--    process (i_clk_50)
--    begin
--        --o_txd <= not tmp;
--        if rising_edge(i_clk_50) then
--            w_cpuClkCt <= w_cpuClkCt + 1;
--
--            if (clken_counter < 49) then
--                clken_counter <= clken_counter + 1;
--            else
--                clken_counter <= (others => '0');
--            end if;
--            
--            if (clken_counter < 25) then
--                mhz1_clken <= '1';
--                Pin42 <= '1';
--            else
--                mhz1_clken <= '0';
--                Pin42 <= '0';
--            end if;
--            if w_cpuClkPh = '0' then
--                w_cpuClk <= '0';
--                Pin40 <= '0';
--            else
--                w_cpuClk <= '1';
--                Pin40 <= '1';
--            end if; 
--
--            if w_cpuClkCt >= 5 and w_cpuClkPh = '0' then -- 12 = 3.85MHz, 49 = 1MHz, 4 = 10MHz, 3 = 12.5MHz, 2=16.6MHz, 1=25MHz
--                w_cpuClkCt <= (others=>'0');
--                w_cpuClkPh <= '1';
--            --else
--                --w_cpuClkCt <= w_cpuClkCt + 1;
--            end if;
--            if w_cpuClkCt >= 5 and w_cpuClkPh = '1' then -- 6 when 4MHz, 25 when 1MHz, 2 when 10MHz, 2 when 12.5MHz, 2 when 16.6MHz, 1 when 25MHz
--                w_cpuClkCt <= (others=>'0');
--                w_cpuClkPh <= '0';
--            --else
--            end if;
--        end if; 
--    end process;

    process (i_clk_50)
    begin
        if rising_edge(i_clk_50) then
            -- 1.8432 MHz clock (ish) from 50 MHz
            if (serialClkCount < 27) then
                serialClkCount <= serialClkCount + 1;
            else
                serialClkCount <= (others => '0');
            end if;
            
            if (serialClkCount < 14) then
                w_ACIAClk <= '1';
            else
                w_ACIAClk <= '0';
            end if;
        end if;
    end process;



  process (mhz1_clken)
  begin
      if (rising_edge(mhz1_clken)) then
        --Data_out <= w_latch_U7;
        LEDR <= not w_latch_U7(1);
        --LEDM <= not w_latch_User(0);
        LEDL <= not w_latch_Bank(0);
        LEDM <= not (w_fKey3 and w_fKey2);
      end if;
  end process;
          
        
    -- ____________________________________________________________________________________
    -- Baud Rate Clock Signals
    -- Serial clock DDS
    -- 50MHz master input clock:
    -- f = (increment x 50,000,000) / 65,536 = 16X baud rate
    -- Baud Increment
    -- 115200 2416
    -- 38400 805
    -- 19200 403
    -- 9600 201
    -- 4800 101
    -- 2400 50

    baud_div: process (w_serClkCt_d, w_serialClkCount, w_fKey2)
        begin
            if w_fKey2 = '0' then
                w_serClkCt_d <= w_serialClkCount + 2416;    -- 115,200 baud
            else
                w_serClkCt_d <= w_serialClkCount + 6;       -- 300 baud
                end if;
        end process;

    --Single clock wide baud rate enable
    baud_clk: process(i_clk_50)
        begin
            if rising_edge(i_clk_50) then
                    w_serialClkCount <= w_serClkCt_d;
                if w_serialClkCount(15) = '0' and w_serClkCt_d(15) = '1' then
                    w_w_serClkEn <= '1';
                    --Pin41 <= '1';
                else
                    w_w_serClkEn <= '0';
                    --Pin41 <= '0';
                end if;
       end if;
    end process;
     
end;
