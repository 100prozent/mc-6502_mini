#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging, sys, argparse, os, string, math, glob
from pathlib import Path
from serial import Serial
import platform
import glob
import time, re

def read(channel, size = 1):
    '''
    Read a sequence of bytes
    '''
    if size == 0:
        return
    result = channel.read(size)
    if len(result) != size:
        raise Exception('I/O error')
    return result
def read_byte(channel):
    '''
    Read a single byte
    '''
    return int(read(channel)[0])

def number_to_linestart(number):
        return ':{:02X}'.format(number)
def changeBank(channel, bank):
    if bank == 0:
        channel.write(str("F8E0:FE\r").encode('ascii'))
    elif (bank == 1): 
        channel.write(str("F8E0:FD\r").encode('ascii'))
    elif (bank == 2): 
        channel.write(str("F8E0:FB\r").encode('ascii'))
    elif (bank == 3): 
        channel.write(str("F8E0:F7\r").encode('ascii'))
    else:
        print("wrong bank")
    l=channel.read(15).decode('ascii')

def checkresponse(channel, type):
    if type == 1:
        l=channel.read(90).decode('ascii')
        line = l.strip()
        print("Response: %s" % line)
        channel.flush()
        if line[-1] == '>':
            return 1
        else:
            return 0
    else:
        l=channel.read(90).decode('ascii')
        line = l.strip()
        print("Response1: %s" % line)
        channel.flush()
        l=channel.read(10).decode('ascii')
        line = l.strip()
        print("Response2: %s" % line)
        channel.flush()
        if line == '>':
            return 1
        else:
            return 0

def upload(channel, f):
    channel.flush()
    with open(f,"r") as file:
        for line in file:
            #data = ''.join(chr(int(x, 16)) )
            ll = (len(line))
            #ll = int((len(line.strip()))/2)
            #print(ll)
            #print(line)
            #channel.write(line)
            #channel.write([58])
            for x in line:
#                #print((int(line[start:start+2], 16)))
                #print(x)
                channel.write(str(x).encode('ascii'))

            r = (channel.read(1).decode('ascii'))

class IntelHEXUploader(object):
    def process_command_line(self):
        ##
        ## Parse command line
        ##
    
        parser = argparse.ArgumentParser(description = 'Command line interface to an Ostrich 2.0 EPROM emulator')
        parser.add_argument(
                '-v', '--verbose',
                action = 'count',
                help = 'increase logging level')

        parser.add_argument(
                '--port',
                metavar = 'DEVICE',
                #default = Ostrich1.default_device(),
                help = 'device [default: %(default)s]')

        subparsers = parser.add_subparsers(
                dest = 'command',
                title = 'IntelHEXUploader operations',
                description = '(See "%(prog)s COMMAND -h" for more info)',
                help = '')

        subparser = subparsers.add_parser(
                'write',
                help = 'uploads data to device')

        subparser.add_argument(
                'file',
                nargs = '?',
                #type = _parse_input_file,
                #default = sys.stdin.buffer,
                help = 'input filename [default: use stdin]')

        subparser = subparsers.add_parser('version',
                                          help = 'display device version number and exit',
                                          add_help = False)
    
        args = parser.parse_args()
    
        if not args.verbose:
            level = logging.ERROR
        elif args.verbose == 1:
            level = logging.INFO
        else:
            level = logging.DEBUG
        logging.basicConfig(stream = sys.stderr, level = level, format = '%(message)s')

        ##
        ## Handle command
        ##
    
        logging.info('Initialized')

        if args.command == 'write':
            self.write_to_device(args.file, args.port)
        
        logging.info('Disconnected from device')

    def write_to_device(self, fn, port):
        fdir = "../fpga/ROMs/"

        print("Port: %s" % (port))
        try:
            channel = Serial(port, 19200, timeout = 20, writeTimeout = 5)
        except:
            print ("Error opening serial port!")
            exit()
        channel.flushInput()
        print("channel open")
        if fn != None:
            print("Filename: %s" % (fn))
        else:
            print("Looking for files in %s" % fdir)
            p0 = fdir + "P0.hex"
            if os.path.exists(p0):
                print("Found P0.hex")
            p1 = fdir + "P1.hex"
            if os.path.exists(p1):
                print("Found P1.hex")
            p2 = fdir + "P2.hex"
            if os.path.exists(p2):
                print("Found P2.hex")
            p3 = fdir + "P3.hex"
            if os.path.exists(p3):
                print("Found P3.hex")
            root = fdir + "root.hex"
            if os.path.exists(root):
                print("Found root.hex")
            tgm = fdir + "tgm.hex"
            if os.path.exists(tgm):
                print("Found tgm.hex")
            sbc = fdir + "sbc.hex"
            if os.path.exists(sbc):
                print("Found sbc.hex")
        channel.flushInput()
        print("Please reset Monitor now....")
        #channel.timeout=20
        #line=channel.readline()
        line=channel.read(31).decode('ascii')
        print("Response: %s" % line.strip())
        print("Monitor should be ready for upload....")
        channel.timeout=0.1
        upload(channel, sbc)
        if checkresponse(channel, 1) == 1:
            print("%s upload OK" % sbc)
        else:
            print("%s failure" % sbc)
        channel.write(str("U\r").encode('ascii'))
        channel.timeout=1
        upload(channel, tgm)
        if checkresponse(channel, 2) == 1:
            print("%s upload OK" % tgm)
        else:
            print("%s failure" % tgm)
        channel.write(str("U\r").encode('ascii'))
        upload(channel, root)
        if checkresponse(channel, 2) == 1:
            print("%s upload OK" % root)
        else:
            print("%s failure" % root)
        time.sleep(1)
        changeBank(channel, 0) 
        channel.write(str("U\r").encode('ascii'))
        upload(channel, p0)
        if checkresponse(channel, 2) == 1:
            print("%s upload OK" % p0)
        else:
            print("%s failure" % p0)
        time.sleep(1)
        changeBank(channel, 1)
        channel.write(str("U\r").encode('ascii'))
        upload(channel, p1)
        if checkresponse(channel, 2) == 1:
            print("%s upload OK" % p1)
        else:
            print("%s failure" % p1)
        time.sleep(1)
        changeBank(channel, 2)
        channel.write(str("U\r").encode('ascii'))
        upload(channel, p2)
        if checkresponse(channel, 2) == 1:
            print("%s upload OK" % p2)
        else:
            print("%s failure" % p2)
        time.sleep(1)
        changeBank(channel, 3)
        channel.write(str("U\r").encode('ascii'))
        upload(channel, p3)
        if checkresponse(channel, 2) == 1:
            print("%s upload OK" % p3)
        else:
            print("%s failure" % p3)
        time.sleep(1)
        changeBank(channel, 0)


        #raise SystemExit

        channel.close()
        print("channel closed")

if __name__ == "__main__":
    IntelHEXUploader().process_command_line()