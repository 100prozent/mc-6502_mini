#include <stdint.h>

uint8_t readACIA(uint16_t address);
void writeACIA(uint16_t address, uint8_t value);
void serial_in_queue_put(uint8_t b);
