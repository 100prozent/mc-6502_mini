#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/ioctl.h> // For FIONREAD
#include <termios.h>
#include <stdbool.h>
#include <time.h>
#include <memory.h>
#include <ctype.h>
#include <unistd.h>
#include "acia.h"

#define ACIAdat 0 // R/W
#define ACIAsta 1 // Read
#define ACIAcmd 2 // Write
#define ACIActl 3 // Write
#define TDRE (1<<4) // Transmitter Empty
#define RDRF (1<<3) // Receiver full

#define SERIAL_IN_QUEUE_SIZE 1024
uint8_t serial_in_queue[SERIAL_IN_QUEUE_SIZE];
int serial_in_queue_start = 0;
int serial_in_queue_end = 0;

int serial_in_queue_full() {
    return serial_in_queue_start != serial_in_queue_end;
}

void serial_in_queue_put(uint8_t b) {
    serial_in_queue[serial_in_queue_end] = b;
    serial_in_queue_end = (serial_in_queue_end + 1) % SERIAL_IN_QUEUE_SIZE;
}

uint8_t serial_in_queue_get() {
    uint8_t b;
    if (serial_in_queue_start == serial_in_queue_end) return 0;
    b = serial_in_queue[serial_in_queue_start];
    serial_in_queue_start = (serial_in_queue_start + 1) % SERIAL_IN_QUEUE_SIZE;
    return b;
}

uint8_t readACIA(uint16_t address) {
	switch(address & 3) {
		case ACIAdat: {
            return serial_in_queue_get();
			break;
        }
		case ACIAsta: {
			if (serial_in_queue_full()){
				//printf("in queue full\n");
				return (TDRE | RDRF);
			} else {
				return TDRE;
			}
			break;
        }
		default:
            break;
	}
	return 0;
}

void writeACIA(uint16_t address, uint8_t value) {
	switch(address & 3) {
		case ACIActl:
			printf("ACIA Control: %0x\n", value);
			break;
		case ACIAcmd:
			printf("ACIA Command: %0x\n", value);
			break;
		case ACIAdat: {
			//printf("ACIA Data: %0x\n", value);
            printf("%c", value);
            fflush(stdout);
			break;
        }
		default:
            break;
	}
}



