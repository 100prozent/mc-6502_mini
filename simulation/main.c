// vim: tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab
/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2010 Gareth McMullin <gareth@blacksphere.co.nz>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/ioctl.h> // For FIONREAD
#include <termios.h>
#include <stdbool.h>
#include <time.h>
#include <memory.h>
#include <ctype.h>
#include <unistd.h>

#include "fake6502.h"
#include "acia.h"
#define ACIA 0xF000
#define ROM 0xFC00
#define max_ram 32768
uint8_t volatile ram[max_ram];
uint8_t rom[1024];

struct timespec last_tick_time;

extern void reset6502();
extern void exec6502(uint32_t);
extern void step6502();
extern void nmi6502();

void load_roms();

void do_step() {
    // Reset the CPU tick count so we can get the number of ticks
    // this instruction took
    step6502();
    clock_gettime(CLOCK_REALTIME, &last_tick_time);
}

void set_raw() {
    static const int STDIN = 0;

    // Use termios to turn off line buffering
    struct termios term;
    tcgetattr(STDIN, &term);
    term.c_lflag &= ~ICANON;
    term.c_lflag &= ~ECHO;
    term.c_iflag &= ~ICRNL;
    tcsetattr(STDIN, TCSANOW, &term);
    setbuf(stdin, NULL);
}

int kbhit(bool init) {
    static bool initflag = false;
    static const int STDIN = 0;

    // If raw mode hasn't been turned on yet, turn it on
    if (init || !initflag) {
        set_raw();
        initflag = true;
    }

    // Return the number of bytes available to read
    int nbbytes;
    ioctl(STDIN, FIONREAD, &nbbytes);  // 0 is STDIN
    return nbbytes;
}

int reset_term() {
    static const int STDIN = 0;

    // Use termios to turn on line buffering
    struct termios term;
    tcgetattr(STDIN, &term);
    term.c_lflag |= ICANON;
    term.c_lflag |= ECHO;
    term.c_iflag |= ICRNL;
    tcsetattr(STDIN, TCSANOW, &term);
    setbuf(stdin, NULL);
}

void handle_kb() {
    char ch;
    ch = getchar();

    if (ch == 'I') {
        printf("Interrupt\n");
        irq6502();
    } else if (ch == 'N') {
        printf("NMI\n");
        nmi6502();
    } else {
    	//printf("In HandleKB\n");
        serial_in_queue_put(ch);
    }
    return;
}

void load_roms() {
    FILE *in;

    if ((in = fopen("../firmware/sbc.bin", "rb")) == NULL) {
        fprintf(stderr, "../firmware/sbc.bin\n");
        exit(1);
    }
    fread(rom, 1, sizeof(rom), in);
    fclose(in);
    printf("../firmware/sbc.bin successfully loaded\n");
}

uint8_t read6502(uint16_t address) {
    if ((address >= ROM) && (address < 0x10000)) {
        return rom[address-ROM];
    } else if ((address >= ACIA) && (address < ROM)) {
        return readACIA(address-ACIA);
    } else if (address < max_ram) {
        return ram[address];
    } else {
        return 0;
    }
}

void write6502(uint16_t address, uint8_t value) {
    if ((address >= ACIA) && (address < ROM)) {
        writeACIA(address - ACIA, value);
    } else if (address < max_ram) {
        ram[address] = value;
    } else {
        printf("Write %02x to %04x\n", value, address);
    }
}

int main(void){
    struct timespec tv, nsleep;

	load_roms();
	reset6502();
    clock_gettime(CLOCK_REALTIME, &last_tick_time);

    for (;;) {

        // Try to simulate a 1MHz clock speed
        clock_gettime(CLOCK_REALTIME, &tv);
        if (tv.tv_sec == last_tick_time.tv_sec) {
            if (tv.tv_nsec - last_tick_time.tv_nsec < 1000) {
                nsleep.tv_sec = 0;
                nsleep.tv_nsec = tv.tv_nsec - last_tick_time.tv_nsec;
                nanosleep(&nsleep, NULL);
            }
        }

        do_step();

        // If a key has been hit, process it
        if (kbhit(false)) {
            handle_kb();
        }
    }
}
