#!/bin/bash
#
#NAME=intelHEX_dl
NAME=sbc
DIR="/home/rob/Projects/mc-6502_mini/"
WD=`pwd`
echo $PWD
cd $DIR/firmware
./ca65 --cpu 6502 -o ${NAME}.o -l ${NAME}.lst ${NAME}.asm
#./ca65 --cpu m740 -o main_reassembled.o -l main.lst main.asm
#./ca65 --cpu m740 -o vetors.o vectors.asm

#./ld65 --start-addr 0xfc00 -o ${NAME}.bin ${NAME}.o
./ld65 -C ${NAME}.cfg -o ${NAME}.bin ${NAME}.o

rm ${NAME}.o
#srec_cat ${NAME}.bin -binary -output ${NAME}.hex -Intel -address_length=2 
srec_cat ${NAME}.bin -binary -offset 0xFC00 -output ${NAME}.hex -Intel -address_length=2 
#srec_cat P0.bin -binary -offset 0xA000 -output P0.hex -Intel -address_length=2 
#srec_cat P1.bin -binary -offset 0xA000 -output P1.hex -Intel -address_length=2 
#srec_cat P2.bin -binary -offset 0xA000 -output P2.hex -Intel -address_length=2 
#srec_cat P3.bin -binary -offset 0xA000 -output P3.hex -Intel -address_length=2 
#srec_cat root.bin -binary -offset 0x8000 -output root.hex -Intel -address_length=2 

#srec_cat sbc.bin -binary -output sbc.hex -Intel -address_length=2 
cp -u ${NAME}.hex ../fpga/ROMs
cd $WD
#Compare
#xxd IMO100.bin > main.hex
#xxd main_reassembled.bin > main_reassembled.hex
#diff main.hex main_reassembled.hex

