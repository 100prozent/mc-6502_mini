;      *= $8002			; create exact 32k bin image

;
; prefill 32k block from $8002-$ffff with 'FF'
;
;      .rept 2047
;         .byte  $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff ;
;      .next 
;      .byte  $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff  ;

;
; compile the sections of the OS
;

	*=$FC00

      	.include ACIA1.asm	   ; ACIA init (19200,n,8,1)

 	.include sbcos.asm         ; OS
 
	.include reset.asm         ; Reset & IRQ handler

