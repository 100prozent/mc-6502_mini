library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

--entity V74x148 is
--	port (
--		EI_L: in STD_LOGIC;
--		I_L:  in STD_LOGIC_VECTOR (7 downto 0);
--		A_L:  out STD_LOGIC_VECTOR (2 downto 0);
--		EO_L: out STD_LOGIC;
--		GS_L: out STD_LOGIC
--	);
--end V74x148;
--
--architecture V74x148p of V74x148 is
--	signal EI: STD_LOGIC;                     -- active-high version of input
--	signal I: STD_LOGIC_VECTOR (7 downto 0);  -- active-high version of inputs
--	signal EO, GS: STD_LOGIC;                 -- active-high version of outputs
--	signal A: STD_LOGIC_VECTOR (2 downto 0);  -- active-high version of outputs
--begin
--	process (EI_L, I_L, EI, EO, GS, I, A)
--	variable j: INTEGER range 7 downto 0;
--	begin
--		EI <= not EI_L; -- convert input
--		I <= not I_L; -- convert inputs
--		EO <= '1';
--		GS <= '0';
--		A <= "000";
--		if (EI)='0' then 
--			EO <= '0';
--		else 
--			for j in 7 downto 0 loop
--				if GS = '1' then null;
--				elsif I(j)='1' then
--					GS <= '1';
--					EO <= '0';
--					A <= CONV_STD_LOGIC_VECTOR(j,3);
--				end if;
--			end loop;
--		end if;
--		EO_L <= not EO; -- convert output
--		GS_L <= not GS; -- convert output
--		A_L <= not A; -- convert outputs
--	end process;
--end V74x148p;

entity V74x148 is
	port (
		--EI_L: in STD_LOGIC;
		I_L: in STD_LOGIC_VECTOR (7 downto 0);
		A_L: out STD_LOGIC_VECTOR (2 downto 0);
		EO_L: out STD_LOGIC
	);
end V74x148;

architecture ARCH of V74x148 is
	--signal I: STD_LOGIC_VECTOR (7 downto 0);  -- active-high version of inputs	
	signal A: STD_LOGIC_VECTOR (2 downto 0);  -- active-high version of outputs
	signal EO: STD_LOGIC;
begin
	process ( I_L)
	begin
		--if rising_edge(EI_L) then
			If    (I_L(0) = '0') then 
				A <= "000";
				EO <= '1';
			elsif (I_L(1) = '0') then 
				A <= "001";
				EO <= '1';
			elsif (I_L(2) = '0') then 
				A <= "010";
				EO <= '1';
			elsif (I_L(3) = '0') then 
				A <= "011";
				EO <= '1';
			elsif (I_L(4) = '0') then 
				A <= "100";
				EO <= '1';
			elsif (I_L(5) = '0') then 
				A <= "101";
				EO <= '1';
			elsif (I_L(6) = '0') then 
				A <= "110";
				EO <= '1';
			elsif (I_L(7) = '0') then 
				A <= "111";
				EO <= '1';
			else A <= "000";
				EO <= '0';
			end if ;
		--end if ;
		A_L <= not A; -- convert output
		EO_L <=  EO; -- convert output
	end process ;
end ARCH;